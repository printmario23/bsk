package tdd.training.bsk;

public class Game {
	private Frame frames[];
	private int nFrames;
	private int firstBonusThrow;
	private int secondBonusThrow;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new Frame[10];
		nFrames = 0;
		firstBonusThrow = 0;
		secondBonusThrow = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.frames[nFrames] = frame; 
		nFrames ++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return this.frames[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow; 
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	private int calculateBonusIfSpare(int i) {								      
		if(i==9) {
			return firstBonusThrow;
		}else{
			return frames[i+1].getFirstThrow();
		}
	}

	private int calculateBonusIfStrike(int i) {		
		if(i==9) {
			return firstBonusThrow + secondBonusThrow;
		}else{
			if(frames[i+1].isStrike()) {
				if(i==8) {
					return frames[i+1].getFirstThrow() + firstBonusThrow;
				}else{
					if(frames[i+2].isStrike()) {
						return frames[i+1].getScore() + frames[i+2].getScore();
					}else {
						return frames[i+1].getScore() + frames[i+2].getFirstThrow();
					}
				}
			}else {
				return frames[i+1].getScore();
			}
		}
	}
	
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(int i=0; i<nFrames; i++) {
			
			if(frames[i].isSpare()){
				frames[i].setBonus(calculateBonusIfSpare(i));
			}else if(frames[i].isStrike()) {                                 
				frames[i].setBonus(calculateBonusIfStrike(i));
			}
			
			score += frames[i].getScore();                                         
		}
		return score;	
	}

}
