package bsk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {
	
	private Game game;
	private Frame frames[];
	
	@Before
	public void setup() throws Exception {
		game = new Game();
		frames = new Frame[10];
		
		frames[0] = null;
		frames[1] = null;
		frames[2] = null;
		frames[3] = null;
		frames[4] = new Frame(4,4);
		frames[5] = new Frame(5,3);
		frames[6] = new Frame(3,3);
		frames[7] = new Frame(4,5);
		frames[8] = new Frame(8,1);
		frames[9] = null;
	}
	
	@Test
	public void testFirstFrameShouldBeOneAndFIve() throws Exception{
		frames[0] = new Frame(1,5);
		frames[1] = new Frame(3,6);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,6);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		assertEquals(frames[0], game.getFrameAt(0));
	}
	
	@Test(expected=ArrayIndexOutOfBoundsException.class)
	public void testGetFrameAtShouldThrowsException() throws Exception{
		frames[0] = new Frame(1,5);
		frames[1] = new Frame(3,6);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,6);

		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		game.getFrameAt(12);
	}
	
	@Test 
	public void testScoreShouldBeEightyOne() throws Exception{
		frames[0] = new Frame(1,5);
		frames[1] = new Frame(3,6);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,6);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		assertEquals(81, game.calculateScore());
	}
	
	@Test 
	public void testScoreShouldBeEightyEight() throws Exception{
		frames[0] = new Frame(1,9);
		frames[1] = new Frame(3,6);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,6);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		assertEquals(88, game.calculateScore());
	}
	
	
	@Test 
	public void testScoreShouldBeNinetyFour() throws Exception{
		frames[0] = new Frame(10,0);
		frames[1] = new Frame(3,6);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,6);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test 
	public void testScoreShouldBeOneHundredThree() throws Exception{
		frames[0] = new Frame(10,0);
		frames[1] = new Frame(4,6);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,6);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test 
	public void testScoreShouldBeOneHundredTwelve() throws Exception{
		frames[0] = new Frame(10,0);
		frames[1] = new Frame(10,0);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,6);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		assertEquals(112, game.calculateScore());
	}
	@Test 
	public void testScoreShouldBeOneHundredTwentyNine() throws Exception{
		frames[0] = new Frame(10,0);
		frames[1] = new Frame(10,0);
		frames[2] = new Frame(10,0);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,6);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		assertEquals(129, game.calculateScore());
	}
	
	@Test 
	public void testScoreShouldBeOneHundredFifty() throws Exception{
		frames[0] = new Frame(10,0);
		frames[1] = new Frame(10,0);
		frames[2] = new Frame(10,0);
		frames[3] = new Frame(10,0);
		frames[9] = new Frame(2,6);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		assertEquals(150, game.calculateScore());
	}
	
	@Test 
	public void testScoreShouldBeOneNinetyEight() throws Exception{
		frames[0] = new Frame(8,2);
		frames[1] = new Frame(5,5);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,6);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testFirstBonusThrowShouldBeFive() throws Exception{
		game.setFirstBonusThrow(5);
		assertEquals(5, game.getFirstBonusThrow());
	}
	
	@Test
	public void testScoreShouldBeNinety() throws Exception{
		frames[0] = new Frame(1,5);
		frames[1] = new Frame(3,6);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(2,8);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testSecondBonusThrowShouldBeTwo() throws Exception{
		game.setSecondBonusThrow(2);
		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	public void testScoreShouldBeNinetyTwo() throws Exception{
		frames[0] = new Frame(1,5);
		frames[1] = new Frame(3,6);
		frames[2] = new Frame(7,2);
		frames[3] = new Frame(3,6);
		frames[9] = new Frame(10,0);
		
		for(int i = 0; i<10; i++) {
			game.addFrame(frames[i]);
		}
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testBestScoreShouldBeThreeHundred() throws Exception{
		for(int i = 0; i < 10; i++) {
			frames[i] = new Frame(10,0);	
			game.addFrame(frames[i]);
		}
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
}
