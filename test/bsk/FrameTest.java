package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testFirstThrowShuldBeTwo() throws Exception{
		Frame frame = new Frame(2,4);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void testSecondThrowShuldBeFour() throws Exception{
		Frame frame = new Frame(2,4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	public void testScoreShouldBeEight() throws Exception{
		Frame frame = new Frame(2,6);
		assertEquals(8, frame.getScore());
	}
	
	@Test(expected=BowlingException.class)
	public void testShouldThrowException() throws Exception{
		new Frame(11,6);
	}
	
	@Test
	public void testBonusShouldBeEight() throws Exception{
		Frame frame = new Frame(5,3);
		frame.setBonus(8);
		assertEquals(8, frame.getBonus());
	}
	
	@Test
	public void testSpareShouldBeTrue() throws Exception{
		Frame frame = new Frame(5,5);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testSpareShouldBeFalse() throws Exception{
		Frame frame = new Frame(3,3);
		assertFalse(frame.isSpare());
	}
	
	@Test
	public void testSpareShouldBeFalseBecauseIsStrike() throws Exception{
		Frame frame = new Frame(10,0);
		assertFalse(frame.isSpare());
	}
	
	@Test
	public void testScoreShouldBeFifteen() throws Exception{
		Frame frame = new Frame(8,2);
		frame.setBonus(5);
		assertEquals(15,frame.getScore());
	}
	
	@Test
	public void testStrikeShouldBeTrue() throws Exception{
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testScoreShouldBeNineteen() throws Exception{
		Frame frame = new Frame(10,0);
		frame.setBonus(9);
		assertEquals(19, frame.getScore());
	}
	
}
